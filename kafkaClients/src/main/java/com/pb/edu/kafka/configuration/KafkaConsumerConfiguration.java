package com.pb.edu.kafka.configuration;

import java.util.Properties;

public class KafkaConsumerConfiguration {

	public static Properties getProperties(NetworkConfiguration inputConfiguration,
			ConsumerConfiguration consumerConfiguration) {
		Properties props = new Properties();
		props.put("bootstrap.servers", inputConfiguration.getServersConfiguration());
		props.put("group.id", consumerConfiguration.getConsumerGroup());
		props.put("client.id", consumerConfiguration.getConsumerId());
		props.put("enable.auto.commit", "false");
		props.put("auto.commit.interval.ms", "1000");
		props.put("session.timeout.ms", "6002");
		props.put("heartbeat.interval.ms", "6001");
		props.put("request.timeout.ms", "6003");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		return props;
	}

}
