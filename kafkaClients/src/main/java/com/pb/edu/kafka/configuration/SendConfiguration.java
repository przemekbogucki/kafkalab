package com.pb.edu.kafka.configuration;

import java.util.Arrays;
import java.util.List;

public class SendConfiguration {

	private String topicName = "testTopic";
	private int maxIterations = 0;
	private int keyModule = 10;
	private int sendSleep = 500;
	private List<String> keys = Arrays.asList("key4", "key3", "key2");

	public void init(int maxIterations, int keyModule, int sendSleep, String topicName) {
		this.maxIterations = maxIterations;
		this.keyModule = keyModule;
		this.sendSleep = sendSleep;
		this.topicName = topicName;
	}

	public int getMaxIterations() {
		return maxIterations;
	}

	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	public int getKeyModule() {
		return keyModule;
	}

	public void setKeyModule(int keyModule) {
		this.keyModule = keyModule;
	}

	public int getSendSleep() {
		return sendSleep;
	}

	public void setSendSleep(int sendSleep) {
		this.sendSleep = sendSleep;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public List<String> getKeys() {
		return keys;
	}

	public void setKeys(List<String> keys) {
		this.keys = keys;
	}

	private StringBuilder getCommonConfiguration() {
		StringBuilder sb = new StringBuilder();
		sb.append("Topic:" + topicName).append(" sleep:" + sendSleep)
				.append(" max iteration:" + (maxIterations == 0 ? "infinitive" : maxIterations));
		return sb;
	}

	public String getRegularConfiguration() {
		return getCommonConfiguration().append(" module:" + keyModule).toString();
	}

	public String getSingleKey() {
		return getCommonConfiguration().append(" keys:" + keys).toString();
	}

}
