package com.pb.edu.kafka.adapter;

import java.util.Arrays;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.pb.edu.kafka.configuration.KafkaProducerConfiguration;
import com.pb.edu.kafka.configuration.NetworkConfiguration;
import com.pb.edu.kafka.configuration.SendConfiguration;

public class KafkaSingleKeyProducer {
	/**
	 * 
	 * @param args iteration, module, sleep
	 */
	public static void main(String[] args) {

		NetworkConfiguration inputConfiguration = new NetworkConfiguration();
		SendConfiguration sendConfiguration = new SendConfiguration();

		if (args.length > 0) {
			try {

				sendConfiguration.setSendSleep(Integer.valueOf(args[0]));
				sendConfiguration.setMaxIterations(Integer.valueOf(args[1]));
				sendConfiguration.setKeys(Arrays.asList(args[2].split(",")));

			} catch (NumberFormatException nfe) {
				System.out.println(
						"Incorrect input format. Expected: sleep(as int), iteration(as int), list of keys with , as sperator");

				return;
			}
		}

		KafkaSingleKeyProducer producer = new KafkaSingleKeyProducer();

		System.out.println("Start Kafka single key producer");
		System.out.println(sendConfiguration.getSingleKey());

		producer.work(inputConfiguration, sendConfiguration);
	}

	public void work(NetworkConfiguration inputConfiguration, SendConfiguration sendConfiguration) {
		Producer<String, String> producer = new KafkaProducer<>(
				KafkaProducerConfiguration.getProperties(inputConfiguration));

		System.out.println(producer.partitionsFor(sendConfiguration.getTopicName()));

		int i = 0;

		while (sendConfiguration.getMaxIterations() == 0 || i < sendConfiguration.getMaxIterations()) {
			try {
				for (String key : sendConfiguration.getKeys()) {
					final String message = "Message id: " + Integer.toString(i);
					producer.send(new ProducerRecord<>(sendConfiguration.getTopicName(), key, message), new Callback() {

						@Override
						public void onCompletion(RecordMetadata metadata, Exception exception) {
							if (exception == null) {
								System.out.println("key:" + key + " , msg:" + message + " - sent to topic:'"
										+ metadata.topic() + "' partition:" + metadata.partition() + " offset:"
										+ metadata.offset() + "");
							} else {
								System.out.println(key + " , " + message + " - ERROR - " + exception);
							}

						}
					});
					i++;
				}
				Thread.sleep(sendConfiguration.getSendSleep());
			} catch (Exception e) {
				break;
			}
		}
		producer.flush();
		producer.close();
		System.out.println("Push done");

	}
}
