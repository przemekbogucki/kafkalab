package com.pb.edu.kafka.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NetworkConfiguration {
	private String IP = "127.0.0.1";

	private List<Pair<String, Integer>> brokers = new ArrayList<Pair<String, Integer>>();

	public NetworkConfiguration() {
		brokers.add(new Pair<String, Integer>(IP, 9091));
		brokers.add(new Pair<String, Integer>(IP, 9092));
		brokers.add(new Pair<String, Integer>(IP, 9093));
	}

	String getServersConfiguration() {
		return brokers.stream().map(pair -> pair.getElement0() + ":" + pair.getElement1())
				.collect(Collectors.joining(","));
	}

	public List<Pair<String, Integer>> getBrokers() {
		return brokers;
	}

	public void setBrokers(List<Pair<String, Integer>> brokers) {
		this.brokers = brokers;
	}
}
