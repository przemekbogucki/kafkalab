package com.pb.edu.kafka.adapter;

import java.util.Arrays;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.pb.edu.kafka.configuration.KafkaProducerConfiguration;
import com.pb.edu.kafka.configuration.NetworkConfiguration;
import com.pb.edu.kafka.configuration.SendConfiguration;

/**
 * Created by apb032 on 2017-09-08.
 */
public class KafkaDataProducer {

	/**
	 * 
	 * @param args sleep, iteration, module
	 */
	public static void main(String[] args) {

		NetworkConfiguration inputConfiguration = new NetworkConfiguration();
		SendConfiguration sendConfiguration = new SendConfiguration();

		if (args.length > 0) {
			int[] intArgs = new int[3];

			try {
				intArgs = Arrays.stream(args).mapToInt(arg -> Integer.valueOf(arg)).toArray();
			} catch (NumberFormatException nfe) {
				System.out
						.println("Incorrect input format. Expected: sleep(as int), iteration(as int), module(as int)");

				return;
			}

			if (args.length > 0) {
				sendConfiguration.setSendSleep(intArgs[0]);
			}
			if (args.length > 1) {
				sendConfiguration.setMaxIterations(intArgs[1]);
			}
			if (args.length > 2) {
				sendConfiguration.setKeyModule(intArgs[2]);
			}
		}

		KafkaDataProducer producer = new KafkaDataProducer();

		System.out.println("Start Kafka producer");
		System.out.println(sendConfiguration.getRegularConfiguration());

		producer.work(inputConfiguration, sendConfiguration);
	}

	public void work(NetworkConfiguration inputConfiguration, SendConfiguration sendConfiguration) {
		Producer<String, String> producer = new KafkaProducer<>(
				KafkaProducerConfiguration.getProperties(inputConfiguration));

		System.out.println(producer.partitionsFor(sendConfiguration.getTopicName()));

		int i = 0;

		while (sendConfiguration.getMaxIterations() == 0 || i < sendConfiguration.getMaxIterations()) {
			try {
				final String key = "Key " + Integer.toString(i % sendConfiguration.getKeyModule());
				final String message = "Message id: " + Integer.toString(i);
				producer.send(new ProducerRecord<>(sendConfiguration.getTopicName(), key, message), new Callback() {

					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception == null) {
							System.out.println("P key:" + key + " , msg:" + message + " - sent to topic:'"
									+ metadata.topic() + "' partition:" + metadata.partition() + " offset:"
									+ metadata.offset() + "");
						} else {
							System.out.println(key + " , " + message + " - ERROR - " + exception);
						}

					}
				});
				Thread.sleep(sendConfiguration.getSendSleep());
			} catch (Exception e) {
				break;
			}
			i++;
		}
		producer.flush();
		producer.close();
		System.out.println("Push done");

	}
}
