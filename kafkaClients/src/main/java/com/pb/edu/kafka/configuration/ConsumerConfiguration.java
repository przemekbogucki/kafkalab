package com.pb.edu.kafka.configuration;

public class ConsumerConfiguration {

	private String topicName = "testTopic";
	private String consumerGroup = "testGroup";
	private String consumerId = "1";

	public String getConsumerGroup() {
		return consumerGroup;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public void setConsumerGroup(String consumerGroup) {
		this.consumerGroup = consumerGroup;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicName() {
		return topicName;
	}

	public String getConfiguration() {
		StringBuilder sb = new StringBuilder();
		sb.append(" consumer id:" + consumerId).append(" consumer group:" + consumerGroup)
				.append(" Topic:" + topicName);
		return sb.toString();
	}
}
