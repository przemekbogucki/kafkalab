package com.pb.edu.kafka.adapter;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import com.pb.edu.kafka.configuration.ConsumerConfiguration;
import com.pb.edu.kafka.configuration.KafkaConsumerConfiguration;
import com.pb.edu.kafka.configuration.NetworkConfiguration;

public class KafkaDataConsumer {

	private Collection<TopicPartition> partitions;

	/**
	 * 
	 * @param args client id, topic name
	 */
	public static void main(String[] args) {
		NetworkConfiguration networkConfiguration = new NetworkConfiguration();
		ConsumerConfiguration configuration = new ConsumerConfiguration();

		if (args.length > 0) {

			if (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("--?")
					|| args[0].equalsIgnoreCase("-?")) {
				System.out.println("Exec params: Consumer Id, Consumer Group, Topic Name");
				return;
			}

			configuration.setConsumerId(args[0]);
		}
		if (args.length > 1) {
			configuration.setConsumerGroup(args[1]);
		}
		if (args.length > 2) {
			configuration.setTopicName(args[2]);
		}

		KafkaDataConsumer consumer = new KafkaDataConsumer();
		System.out.println("Start Kafka Consumer...");
		System.out.println(configuration.getConfiguration());

		consumer.work(networkConfiguration, configuration);
	}

	private void work(NetworkConfiguration networkConfiguration, ConsumerConfiguration consumerConfiguration) {

		KafkaConsumer<String, String> consumer = null;

		try {
			consumer = new KafkaConsumer<>(
					KafkaConsumerConfiguration.getProperties(networkConfiguration, consumerConfiguration));
			consumer.subscribe(Arrays.asList(consumerConfiguration.getTopicName()), new ConsumerRebalancer());

			while (true) {
				try {
					ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(4000));

					if (!records.isEmpty()) {
						for (ConsumerRecord<String, String> record : records) {
							System.out.println("C key:" + record.key() + " , msg:" + record.value()
									+ " - get from topic:'" + record.topic() + "' partition:" + record.partition()
									+ " offset:" + record.offset() + "");
						}
					} else {
						System.out.println("C no new data");
					}
					consumer.commitSync();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} finally {
			if (consumer != null) {
				consumer.close();
			}
		}
	}

	private class ConsumerRebalancer implements ConsumerRebalanceListener {

		@Override
		public void onPartitionsRevoked(Collection<TopicPartition> _partitions) {
			partitions = _partitions;
			System.out.println("Repartition detected:" + partitions);
		}

		@Override
		public void onPartitionsAssigned(Collection<TopicPartition> _partitions) {
			partitions = _partitions;
			System.out.println("Repartition detected:" + partitions);
		}
	}

}
