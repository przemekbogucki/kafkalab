package com.pb.edu.kafka.configuration;

import java.util.Properties;

public class KafkaProducerConfiguration {

	public static Properties getProperties(NetworkConfiguration inputConfiguration) {
		Properties props = new Properties();
		props.put("bootstrap.servers", inputConfiguration.getServersConfiguration());
		props.put("acks", "all");
		props.put("timeout.ms", 3000);
		props.put("retries", 0);
		props.put("batch.size", 16);
		props.put("linger.ms", 5);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		return props;
	}
}
